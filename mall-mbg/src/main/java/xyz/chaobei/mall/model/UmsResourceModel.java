package xyz.chaobei.mall.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 资源管理实体
 *
 * @author mrc
 * @since 2021-01-13 11:11:20
 */
@Data
@TableName("ums_resource")
public class UmsResourceModel implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 资源表
     */
    @TableId(value = "`id`",type = IdType.AUTO)
    private Integer id;

    /**
     * 资源分类ID
     */
    @TableField("`category_id`")
    private Integer categoryId;

    /**
     * 资源名称
     */
    @TableField("`name`")
    private String name;

    /**
     * 资源路径
     */
    @TableField("`url`")
    private String url;

    /**
     * 资源描述
     */
    @TableField("`description`")
    private String description;

    /**
     * 创建时间
     */
    @TableField(value = "`create_time`",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField("`status`")
    private Integer status;

}
