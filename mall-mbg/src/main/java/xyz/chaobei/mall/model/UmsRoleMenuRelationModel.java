package xyz.chaobei.mall.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 角色开通的菜单实体
 *
 * @author mrc
 * @since 2021-01-19 10:10:17
 */
@Data
@TableName("ums_role_menu_relation")
public class UmsRoleMenuRelationModel implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 角色与菜单关联表
     */
    @TableId(value = "`id`",type = IdType.AUTO)
    private Integer id;

    /**
     * 角色id
     */
    @TableField("`role_id`")
    private Integer roleId;

    /**
     * 菜单id
     */
    @TableField("`menu_id`")
    private Integer menuId;

    /**
     * 
     */
    @TableField(value = "`create_time`",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField("`status`")
    private Integer status;

}
