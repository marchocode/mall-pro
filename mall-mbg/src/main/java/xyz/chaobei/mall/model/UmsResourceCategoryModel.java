package xyz.chaobei.mall.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 资源分类实体
 *
 * @author mrc
 * @since 2021-03-15 17:01:57
 */
@Data
@TableName("ums_resource_category")
public class UmsResourceCategoryModel implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 
     */
    @TableId(value = "`id`",type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    @TableField("`name`")
    private String name;

    /**
     * 
     */
    @TableField(value = "`create_time`",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField("`status`")
    private Integer status;

}
