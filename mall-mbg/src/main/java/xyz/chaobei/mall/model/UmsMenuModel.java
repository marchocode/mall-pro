package xyz.chaobei.mall.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 菜单管理实体
 *
 * @author mrc
 * @since 2021-01-15 16:30:41
 */
@Data
@TableName("ums_menu")
public class UmsMenuModel implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 菜单表
     */
    @TableId(value = "`id`",type = IdType.AUTO)
    private Integer id;

    /**
     * 父级ID
     */
    @TableField("`parent_id`")
    private Integer parentId;

    /**
     * 菜单标题
     */
    @TableField("`title`")
    private String title;

    /**
     * 菜单级别
     */
    @TableField("`level`")
    private Integer level;

    /**
     * 菜单排序
     */
    @TableField("`sort`")
    private Object sort;

    /**
     * 前端VUE 名称
     */
    @TableField("`name`")
    private String name;

    /**
     * 图标
     */
    @TableField("`icon`")
    private String icon;

    /**
     * 是否隐藏 0隐藏 1展示
     */
    @TableField("`hidden`")
    private Integer hidden;

    /**
     * 
     */
    @TableField(value = "`create_time`",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 逻辑删除
     */
    @TableField("`status`")
    private Integer status;

}
