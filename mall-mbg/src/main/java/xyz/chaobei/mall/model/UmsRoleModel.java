package xyz.chaobei.mall.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 角色管理实体
 *
 * @author mrc
 * @since 2020-10-29 14:06:03
 */
@Data
@TableName("ums_role")
public class UmsRoleModel implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 角色表
     */
    @TableId(value = "`id`",type = IdType.AUTO)
    private Integer id;

    /**
     * 角色名称
     */
    @TableField("`name`")
    private String name;

    /**
     * 角色描述
     */
    @TableField("`description`")
    private String description;

    /**
     * 后台用户数量
     */
    @TableField("`admin_count`")
    private Object adminCount;

    /**
     * 0锁定 1正常使用
     */
    @TableField("`lock`")
    private Integer lock;

    /**
     * 排序
     */
    @TableField("`sort`")
    private Integer sort;

    /**
     * 创建时间
     */
    @TableField(value = "`create_time`",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 逻辑删除状态0 1正常
     */
    @TableField("`status`")
    private Integer status;

}
