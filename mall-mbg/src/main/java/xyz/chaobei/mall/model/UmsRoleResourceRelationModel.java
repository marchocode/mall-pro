package xyz.chaobei.mall.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 角色关联资源实体
 *
 * @author mrc
 * @since 2021-01-13 14:40:21
 */
@Data
@TableName("ums_role_resource_relation")
public class UmsRoleResourceRelationModel implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 
     */
    @TableId(value = "`id`",type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    @TableField("`role_id`")
    private Integer roleId;

    /**
     * 
     */
    @TableField("`resource_id`")
    private Integer resourceId;

    /**
     * 
     */
    @TableField(value = "`create_time`",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField("`status`")
    private Integer status;

}
