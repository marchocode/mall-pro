package xyz.chaobei.mall.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 角色关联权限实体
 *
 * @author mrc
 * @since 2020-10-29 17:32:03
 */
@Data
@TableName("ums_role_permission_relation")
public class UmsRolePermissionRelationModel implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 角色权限关联表
     */
    @TableId(value = "`id`",type = IdType.AUTO)
    private Integer id;

    /**
     * 角色编号
     */
    @TableField("`role_id`")
    private Integer roleId;

    /**
     * 权限编号
     */
    @TableField("`permission_id`")
    private Integer permissionId;

    /**
     * 
     */
    @TableField(value = "`create_time`",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField("`status`")
    private Integer status;

}
