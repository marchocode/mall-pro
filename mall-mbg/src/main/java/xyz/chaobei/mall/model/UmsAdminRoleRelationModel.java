package xyz.chaobei.mall.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 用户关联角色实体
 *
 * @author mrc
 * @since 2020-10-29 16:36:51
 */
@Data
@TableName("ums_admin_role_relation")
public class UmsAdminRoleRelationModel implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 用户角色关联
     */
    @TableId(value = "`id`",type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    @TableField("`admin_id`")
    private Integer adminId;

    /**
     * 角色id
     */
    @TableField("`role_id`")
    private Integer roleId;

    /**
     * 
     */
    @TableField(value = "`create_time`",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField("`status`")
    private Integer status;

}
