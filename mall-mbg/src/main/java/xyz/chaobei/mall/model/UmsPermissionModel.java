package xyz.chaobei.mall.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 权限管理实体
 *
 * @author mrc
 * @since 2020-10-29 17:16:37
 */
@Data
@TableName("ums_permission")
public class UmsPermissionModel implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 权限表
     */
    @TableId(value = "`id`",type = IdType.AUTO)
    private Integer id;

    /**
     * 权限名称
     */
    @TableField("`name`")
    private String name;

    /**
     * 权限字符
     */
    @TableField("`permission`")
    private String permission;

    /**
     * 权限分类
     */
    @TableField("`category`")
    private Integer category;

    /**
     * 
     */
    @TableField(value = "`create_time`",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 
     */
    @TableField("`status`")
    private Integer status;

}
