package xyz.chaobei.mall.mapper;

import xyz.chaobei.mall.model.UmsAdminRoleRelationModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户关联角色dao层
 *
 * @author mrc
 * @since 2020-10-29 16:36:51
 */
public interface UmsAdminRoleRelationMapper extends BaseMapper<UmsAdminRoleRelationModel> {

}