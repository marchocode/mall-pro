package xyz.chaobei.mall.mapper;

import xyz.chaobei.mall.model.UmsPermissionModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 权限管理dao层
 *
 * @author mrc
 * @since 2020-10-29 17:16:37
 */
public interface UmsPermissionMapper extends BaseMapper<UmsPermissionModel> {

}