package xyz.chaobei.mall.mapper;

import xyz.chaobei.mall.model.UmsResourceModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 资源管理dao层
 *
 * @author mrc
 * @since 2021-01-13 11:11:20
 */
public interface UmsResourceMapper extends BaseMapper<UmsResourceModel> {

}