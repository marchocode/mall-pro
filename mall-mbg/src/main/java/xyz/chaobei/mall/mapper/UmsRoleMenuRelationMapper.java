package xyz.chaobei.mall.mapper;

import xyz.chaobei.mall.model.UmsRoleMenuRelationModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色开通的菜单dao层
 *
 * @author mrc
 * @since 2021-01-19 10:10:17
 */
public interface UmsRoleMenuRelationMapper extends BaseMapper<UmsRoleMenuRelationModel> {

}