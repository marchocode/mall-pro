package xyz.chaobei.mall.mapper;

import xyz.chaobei.mall.model.UmsAdminModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 后台用户dao层
 *
 * @author mrc
 * @since 2020-10-11 20:39:11
 */
public interface UmsAdminMapper extends BaseMapper<UmsAdminModel> {

}