package xyz.chaobei.mall.mapper;

import xyz.chaobei.mall.model.UmsRoleModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色管理dao层
 *
 * @author mrc
 * @since 2020-10-29 14:06:03
 */
public interface UmsRoleMapper extends BaseMapper<UmsRoleModel> {

}