package xyz.chaobei.mall.mapper;

import xyz.chaobei.mall.model.UmsRolePermissionRelationModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色关联权限dao层
 *
 * @author mrc
 * @since 2020-10-29 17:32:03
 */
public interface UmsRolePermissionRelationMapper extends BaseMapper<UmsRolePermissionRelationModel> {

}