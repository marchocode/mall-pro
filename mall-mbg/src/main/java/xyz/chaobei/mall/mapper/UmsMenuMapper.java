package xyz.chaobei.mall.mapper;

import xyz.chaobei.mall.model.UmsMenuModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 菜单管理dao层
 *
 * @author mrc
 * @since 2021-01-15 16:30:41
 */
public interface UmsMenuMapper extends BaseMapper<UmsMenuModel> {

}