package xyz.chaobei.mall.mapper;

import xyz.chaobei.mall.model.UmsRoleResourceRelationModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色关联资源dao层
 *
 * @author mrc
 * @since 2021-01-13 14:40:21
 */
public interface UmsRoleResourceRelationMapper extends BaseMapper<UmsRoleResourceRelationModel> {

}