package xyz.chaobei.mall.mapper;

import xyz.chaobei.mall.model.UmsResourceCategoryModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 资源分类dao层
 *
 * @author mrc
 * @since 2021-03-15 17:01:57
 */
public interface UmsResourceCategoryMapper extends BaseMapper<UmsResourceCategoryModel> {

}