package xyz.chaobei.mall.common.enums;

import lombok.Getter;

/**
 * @author MRC
 */
@Getter
public enum HiddenEnums {

    SHOW(1, "展示"),
    HIDDEN(0, "隐藏");

    private final Integer value;

    private final String desc;

    HiddenEnums(Integer val, String desc) {
        this.value = val;
        this.desc = desc;
    }


}
