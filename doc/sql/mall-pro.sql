/*
 Navicat Premium Data Transfer

 Source Server         : docker-mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : 192.168.181.4:3306
 Source Schema         : mall-pro

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 12/03/2021 18:02:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ums_admin
-- ----------------------------
DROP TABLE IF EXISTS `ums_admin`;
CREATE TABLE `ums_admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '后台管理用户',
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `icon` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '头像',
  `lock` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0锁定1正常使用',
  `email` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '电子邮箱',
  `nick_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '昵称',
  `note` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '逻辑删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ums_admin
-- ----------------------------
INSERT INTO `ums_admin` VALUES (1, 'admin', '$2a$10$08arRlZRspTqMBK1N8NqW.9CQq7KWffa47MGelgJMuPK/uXtKX3O6', '#e', 1, 'maruichao@gmail.com', '管理员', '测试', '2020-10-22 16:14:33', '2020-10-22 16:14:36', 1);

-- ----------------------------
-- Table structure for ums_admin_role_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_admin_role_relation`;
CREATE TABLE `ums_admin_role_relation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户角色关联',
  `admin_id` int(11) NOT NULL COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `create_time` datetime(0) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ums_admin_role_relation
-- ----------------------------
INSERT INTO `ums_admin_role_relation` VALUES (1, 1, 1, '2020-10-29 16:46:00', 1);

-- ----------------------------
-- Table structure for ums_menu
-- ----------------------------
DROP TABLE IF EXISTS `ums_menu`;
CREATE TABLE `ums_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单表',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父级ID',
  `title` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单标题',
  `level` tinyint(1) NOT NULL DEFAULT 1 COMMENT '菜单级别',
  `sort` smallint(6) NOT NULL DEFAULT 0 COMMENT '菜单排序',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '前端VUE 名称',
  `icon` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图标',
  `hidden` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否隐藏 0隐藏 1展示',
  `create_time` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ums_menu
-- ----------------------------
INSERT INTO `ums_menu` VALUES (1, 0, '系统管理', 1, 0, 'ums', '#icon', 1, '2021-03-11 16:44:40', 1);
INSERT INTO `ums_menu` VALUES (2, 1, '用户管理', 2, 0, 'admin', '#icon', 1, '2021-03-11 16:44:40', 1);
INSERT INTO `ums_menu` VALUES (3, 1, '角色管理', 2, 0, 'role', '#icon', 1, '2021-03-11 16:44:40', 1);
INSERT INTO `ums_menu` VALUES (4, 1, '角色菜单', 2, 0, 'menu', '#icon', 1, '2021-03-11 16:44:40', 1);

-- ----------------------------
-- Table structure for ums_permission
-- ----------------------------
DROP TABLE IF EXISTS `ums_permission`;
CREATE TABLE `ums_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '权限表',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限名称',
  `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限字符',
  `category` int(11) NOT NULL COMMENT '权限分类',
  `create_time` datetime(0) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ums_permission
-- ----------------------------
INSERT INTO `ums_permission` VALUES (1, '用户添加', 'umsAdmin:save', 1, '2020-10-30 09:54:07', 1);
INSERT INTO `ums_permission` VALUES (2, '用户修改', 'umsAdmin:update', 1, '2020-10-30 09:54:29', 1);
INSERT INTO `ums_permission` VALUES (3, '用户删除', 'umsAdmin:delete', 1, '2020-10-30 09:56:59', 1);
INSERT INTO `ums_permission` VALUES (4, '用户查询', 'umsAdmin:query', 1, '2020-10-30 09:57:24', 1);
INSERT INTO `ums_permission` VALUES (6, '角色添加', 'umsRole:save', 2, '2020-10-30 13:31:34', 1);
INSERT INTO `ums_permission` VALUES (7, '角色修改', 'umsRole:update', 2, '2020-10-30 13:31:54', 1);
INSERT INTO `ums_permission` VALUES (8, '角色删除', 'umsRole:delete', 2, '2020-10-30 13:32:20', 1);
INSERT INTO `ums_permission` VALUES (9, '角色查询', 'umsRole:query', 2, '2020-10-30 13:33:08', 1);

-- ----------------------------
-- Table structure for ums_resource
-- ----------------------------
DROP TABLE IF EXISTS `ums_resource`;
CREATE TABLE `ums_resource`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '资源表',
  `category_id` int(11) NOT NULL COMMENT '资源分类ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源名称',
  `url` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源路径',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源描述',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ums_resource
-- ----------------------------
INSERT INTO `ums_resource` VALUES (1, 1, '用户接口', '/umsAdmin/**', '用户所有权限', '2021-01-13 14:25:55', 1);
INSERT INTO `ums_resource` VALUES (2, 1, '角色接口', '/umsRole/**', '角色所有接口', '2021-01-13 14:26:43', 1);

-- ----------------------------
-- Table structure for ums_role
-- ----------------------------
DROP TABLE IF EXISTS `ums_role`;
CREATE TABLE `ums_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色表',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `description` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色描述',
  `admin_count` smallint(6) NOT NULL DEFAULT 0 COMMENT '后台用户数量',
  `lock` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0锁定 1正常使用',
  `sort` tinyint(4) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_time` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '逻辑删除状态0 1正常',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ums_role
-- ----------------------------
INSERT INTO `ums_role` VALUES (1, '系统管理员', '系统管理员具有最高权限', 0, 1, 0, '2020-09-24 17:15:06', 1);
INSERT INTO `ums_role` VALUES (2, '普通管理员', '普通管理员一般权限', 0, 1, 0, '2020-10-30 09:53:27', 1);
INSERT INTO `ums_role` VALUES (3, '测试管理', '测试', 0, 1, 0, '2021-02-24 11:15:28', 1);

-- ----------------------------
-- Table structure for ums_role_menu_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_role_menu_relation`;
CREATE TABLE `ums_role_menu_relation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色与菜单关联表',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `menu_id` int(11) NOT NULL COMMENT '菜单id',
  `create_time` datetime(0) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ums_role_menu_relation
-- ----------------------------
INSERT INTO `ums_role_menu_relation` VALUES (1, 1, 1, '2021-01-19 10:36:52', 1);
INSERT INTO `ums_role_menu_relation` VALUES (2, 1, 2, '2021-01-19 10:39:23', 1);
INSERT INTO `ums_role_menu_relation` VALUES (3, 1, 3, '2021-01-21 10:59:36', 1);
INSERT INTO `ums_role_menu_relation` VALUES (4, 1, 4, '2021-03-12 14:40:40', 1);

-- ----------------------------
-- Table structure for ums_role_permission_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_role_permission_relation`;
CREATE TABLE `ums_role_permission_relation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色权限关联表',
  `role_id` int(11) NOT NULL COMMENT '角色编号',
  `permission_id` int(11) NOT NULL COMMENT '权限编号',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ums_role_permission_relation
-- ----------------------------
INSERT INTO `ums_role_permission_relation` VALUES (1, 1, 1, '2020-10-30 10:02:09', 1);
INSERT INTO `ums_role_permission_relation` VALUES (2, 1, 2, '2021-01-21 17:37:24', 1);

-- ----------------------------
-- Table structure for ums_role_resource_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_role_resource_relation`;
CREATE TABLE `ums_role_resource_relation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ums_role_resource_relation
-- ----------------------------
INSERT INTO `ums_role_resource_relation` VALUES (1, 1, 1, '2021-01-13 14:26:56', 1);
INSERT INTO `ums_role_resource_relation` VALUES (2, 1, 2, '2021-01-21 17:38:43', 1);

SET FOREIGN_KEY_CHECKS = 1;
