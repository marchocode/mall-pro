package xyz.chaobei.mall.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Copyright (C), 2015-2020
 * FileName: MyBatisPlusConfig
 * Author:   MRC
 * Date:     2020/9/24 13:32
 * Description: mybatis plus 的配置
 * 开启事务支持
 * History:
 */
@Configuration
@EnableTransactionManagement
@MapperScan({"xyz.chaobei.mall.dao", "xyz.chaobei.mall.mapper"})
public class MyBatisPlusConfig {

    /**
     * <p>按照官网提示，打开分页统计
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @return com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor
     * @link https://mp.baomidou.com/guide/page.html
     * @since 2021/1/14
     **/
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        paginationInterceptor.setCountSqlParser(new JsqlParserCountOptimize(true));
        return paginationInterceptor;
    }

}
