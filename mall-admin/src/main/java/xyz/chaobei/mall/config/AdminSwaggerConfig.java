package xyz.chaobei.mall.config;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import xyz.chaobei.mall.common.config.BaseSwaggerConfig;
import xyz.chaobei.mall.common.domain.SwaggerProperties;

/**
 * @copyright (C), 2015-2020
 * @fileName: AdminSwaggerConfig
 * @author: MRC
 * @date: 2020/10/11 21:35
 * @description: 后台接口文档配置信息
 */
@Configuration
@EnableSwagger2
public class AdminSwaggerConfig extends BaseSwaggerConfig {

    @Override
    public SwaggerProperties customSwagger() {
        return SwaggerProperties.builder()
                .title("mall-pro")
                .description("mall-pro 接口描述信息")
                .apiBasePackage("xyz.chaobei.mall.controller")
                .contactName("mrc")
                .enableSecurity(false)
                .version("1.0")
                .build();
    }
}
