package xyz.chaobei.mall.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.chaobei.common.exception.Asserts;
import xyz.chaobei.mall.common.enums.IntEnums;
import xyz.chaobei.mall.mapper.UmsRoleMapper;
import xyz.chaobei.mall.model.UmsRoleModel;
import xyz.chaobei.mall.pojo.UmsRolePageAO;
import xyz.chaobei.mall.pojo.UmsRoleSaveAO;
import xyz.chaobei.mall.service.UmsAdminRoleRelationService;
import xyz.chaobei.mall.service.UmsRoleService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 角色管理 Service impl
 *
 * @author mrc
 * @since 2020-10-29 14:06:03
 */
@Service
public class UmsRoleServiceimpl implements UmsRoleService {

    @Autowired
    private UmsRoleMapper umsRoleMapper;

    @Autowired
    private UmsAdminRoleRelationService umsAdminRoleRelationService;

    @Override
    public List<UmsRoleModel> findAll() {
        return umsRoleMapper.selectList(null);
    }

    @Override
    public UmsRoleModel findOne(Integer id, boolean isExc) {

        UmsRoleModel result = umsRoleMapper.selectById(id);

        if (Objects.isNull(result) && isExc) {
            Asserts.fail("未查询到相关信息");
        }
        return result;
    }

    @Override
    public Page<UmsRoleModel> findPage(UmsRolePageAO pageAO) {

        Page page = new Page(pageAO.getCurrent(), pageAO.getSize());
        QueryWrapper wrapper = new QueryWrapper();

        umsRoleMapper.selectPage(page, wrapper);

        return page;
    }

    @Override
    public boolean save(UmsRoleSaveAO params) {

        UmsRoleModel model = new UmsRoleModel();
        BeanUtils.copyProperties(params, model);
        /**
         * 你的逻辑写在这里
         */
        int num = umsRoleMapper.insert(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean updateById(UmsRoleSaveAO params, Integer id) {

        UmsRoleModel model = new UmsRoleModel();
        BeanUtils.copyProperties(params, model);

        /**
         * 你的逻辑写在这里
         */
        model.setId(id);
        int num = umsRoleMapper.updateById(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean deleteById(Integer id) {

        /**
         * 你的逻辑写在这里
         */
        int num = umsRoleMapper.deleteById(id);
        return SqlHelper.retBool(num);
    }

    @Override
    public List<UmsRoleModel> queryNormalRoleByUserId(Integer userId) {

        List<Integer> roleIds = umsAdminRoleRelationService.queryAdminRoleIds(userId);

        if (roleIds.isEmpty()) {
            return new ArrayList<>();
        }

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("`lock`", IntEnums.UMS_ROLE_UN_LOCK.getVal());
        wrapper.in("`id`", roleIds);

        return umsRoleMapper.selectList(wrapper);
    }

}
