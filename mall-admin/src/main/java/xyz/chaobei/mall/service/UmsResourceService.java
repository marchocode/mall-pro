package xyz.chaobei.mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import xyz.chaobei.mall.model.UmsResourceModel;
import xyz.chaobei.mall.pojo.UmsResourceSaveAO;
import xyz.chaobei.mall.pojo.UmsResourcePageAO;

import java.util.List;

/**
 * 资源管理 Service
 *
 * @author mrc
 * @since 2021-01-13 11:11:20
 */
public interface UmsResourceService {

    /**
     * <p>查询所有资源管理
     * <p>author: mrc
     *
     * @return java.util.List<xyz.chaobei.mall.model.UmsResourceModel>
     * @since 2021-01-13 11:11:20
     **/
    List<UmsResourceModel> findAll();

    /**
     * <p>查询单个资源管理
     * <p>author: mrc
     *
     * @return xyz.chaobei.mall.model.UmsResourceModel
     * @since 2021-01-13 11:11:20
     **/
    UmsResourceModel findOne(Integer id, boolean isExc);

    /**
     * <p>默认分页请求资源管理
     * <p>author: mrc
     *
     * @param pageAO 分页入参查询参数
     * @return xyz.chaobei.mall.model.UmsResourceModel
     * @since 2021-01-13 11:11:20
     **/
    Page<UmsResourceModel> findPage(UmsResourcePageAO pageAO);

    /**
     * <p>保存一个资源管理
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @return boolean
     * @since 2021-01-13 11:11:20
     **/
    boolean save(UmsResourceSaveAO params);

    /**
     * <p>修改一个资源管理
     * <p>author: mrc
     *
     * @param id     被修改的ID 信息
     * @param params 被修改的信息
     * @return boolean
     * @since 2021-01-13 11:11:20
     **/
    boolean updateById(UmsResourceSaveAO params, Integer id);

    /**
     * <p>删除一个资源管理
     * <p>author: mrc
     *
     * @param id 被删除的ID 信息
     * @return boolean
     * @since 2021-01-13 11:11:20
     **/
    boolean deleteById(Integer id);

    /**
     * <p>通过编号集合查询
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param resourceIds id 集合
     * @return java.util.List<java.lang.String>
     * @since 2021/1/13
     **/
    List<String> queryResourceByIds(List<Integer> resourceIds);
    /**
     * <p>按照分类ID查询
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param category 分类
     * @return java.util.List<xyz.chaobei.mall.model.UmsResourceModel>
     * @since 2021/3/16
     **/
    List<UmsResourceModel> queryResourceByCategory(Integer category);
}
