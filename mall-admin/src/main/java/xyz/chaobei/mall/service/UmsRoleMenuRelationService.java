package xyz.chaobei.mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.transaction.annotation.Transactional;
import xyz.chaobei.mall.model.UmsRoleMenuRelationModel;
import xyz.chaobei.mall.pojo.UmsRoleMenuRelationSaveAO;
import xyz.chaobei.mall.pojo.UmsRoleMenuRelationPageAO;

import java.util.List;

/**
 * 角色开通的菜单 Service
 *
 * @author mrc
 * @since 2021-01-19 10:10:17
 */
public interface UmsRoleMenuRelationService {

    /**
     * <p>通过角色ID查询开通的菜单信息
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param ids 角色ID
     * @return java.util.List<xyz.chaobei.mall.model.UmsRoleMenuRelationModel>
     * @since 2021/1/19
     **/
    List<UmsRoleMenuRelationModel> queryByRoleIds(List<Integer> ids);

    /**
     * <p>查询所有角色开通的菜单
     * <p>author: mrc
     *
     * @return java.util.List<xyz.chaobei.mall.model.UmsRoleMenuRelationModel>
     * @since 2021-01-19 10:10:17
     **/
    List<UmsRoleMenuRelationModel> findAll();

    /**
     * <p>查询单个角色开通的菜单
     * <p>author: mrc
     *
     * @return xyz.chaobei.mall.model.UmsRoleMenuRelationModel
     * @since 2021-01-19 10:10:17
     **/
    UmsRoleMenuRelationModel findOne(Integer id, boolean isExc);

    /**
     * <p>默认分页请求角色开通的菜单
     * <p>author: mrc
     *
     * @param pageAO 分页入参查询参数
     * @return xyz.chaobei.mall.model.UmsRoleMenuRelationModel
     * @since 2021-01-19 10:10:17
     **/
    Page<UmsRoleMenuRelationModel> findPage(UmsRoleMenuRelationPageAO pageAO);

    /**
     * <p>保存一个角色开通的菜单
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @return boolean
     * @since 2021-01-19 10:10:17
     **/
    boolean save(UmsRoleMenuRelationSaveAO params);

    /**
     * <p>修改一个角色开通的菜单
     * <p>author: mrc
     *
     * @param id     被修改的ID 信息
     * @param params 被修改的信息
     * @return boolean
     * @since 2021-01-19 10:10:17
     **/
    boolean updateById(UmsRoleMenuRelationSaveAO params, Integer id);

    /**
     * <p>删除一个角色开通的菜单
     * <p>author: mrc
     *
     * @param id 被删除的ID 信息
     * @return boolean
     * @since 2021-01-19 10:10:17
     **/
    boolean deleteById(Integer id);

    /**
     * <p>删除多个角色已配置的权限
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param id   角色ID
     * @param menu 菜单
     * @return boolean
     * @since 2021/3/17
     **/
    @Transactional(rollbackFor = Exception.class)
    boolean updateRoleList(Integer id, List<Integer> menu);
}
