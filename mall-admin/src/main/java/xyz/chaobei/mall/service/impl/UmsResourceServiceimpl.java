package xyz.chaobei.mall.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.chaobei.common.WrapperBuilder;
import xyz.chaobei.common.exception.Asserts;
import xyz.chaobei.mall.mapper.UmsResourceMapper;
import xyz.chaobei.mall.model.UmsResourceCategoryModel;
import xyz.chaobei.mall.model.UmsResourceModel;
import xyz.chaobei.mall.pojo.UmsResourcePageAO;
import xyz.chaobei.mall.pojo.UmsResourceSaveAO;
import xyz.chaobei.mall.service.UmsResourceCategoryService;
import xyz.chaobei.mall.service.UmsResourceService;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 资源管理 Service impl
 *
 * @author mrc
 * @since 2021-01-13 11:11:20
 */
@Service
public class UmsResourceServiceimpl implements UmsResourceService {

    @Autowired
    private UmsResourceMapper umsResourceMapper;
    @Autowired
    private UmsResourceCategoryService umsResourceCategoryService;

    @Override
    public List<UmsResourceModel> findAll() {
        return umsResourceMapper.selectList(null);
    }

    @Override
    public UmsResourceModel findOne(Integer id, boolean isExc) {

        UmsResourceModel result = umsResourceMapper.selectById(id);

        if (Objects.isNull(result) && isExc) {
            Asserts.fail("未查询到相关信息");
        }
        return result;
    }

    @Override
    public Page<UmsResourceModel> findPage(UmsResourcePageAO pageAO) {

        Page page = new Page(pageAO.getCurrent(), pageAO.getSize());

        QueryWrapper wrapper = WrapperBuilder.simpleQuery(pageAO);

        umsResourceMapper.selectPage(page, wrapper);

        return page;
    }

    @Override
    public boolean save(UmsResourceSaveAO params) {

        UmsResourceModel model = new UmsResourceModel();
        BeanUtils.copyProperties(params, model);
        /**
         * 你的逻辑写在这里
         */
        int num = umsResourceMapper.insert(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean updateById(UmsResourceSaveAO params, Integer id) {

        UmsResourceModel model = new UmsResourceModel();
        BeanUtils.copyProperties(params, model);

        /**
         * 你的逻辑写在这里
         */
        model.setId(id);
        int num = umsResourceMapper.updateById(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean deleteById(Integer id) {

        /**
         * 你的逻辑写在这里
         */
        int num = umsResourceMapper.deleteById(id);
        return SqlHelper.retBool(num);
    }

    @Override
    public List<String> queryResourceByIds(List<Integer> resourceIds) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.in("`id`", resourceIds);

        List<UmsResourceModel> result = umsResourceMapper.selectList(wrapper);

        return result.stream().map(item -> item.getId() + ":" + item.getName()).collect(Collectors.toList());
    }

    @Override
    public List<UmsResourceModel> queryResourceByCategory(Integer category) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.in("`category_id`", category);

        return umsResourceMapper.selectList(wrapper);
    }


}
