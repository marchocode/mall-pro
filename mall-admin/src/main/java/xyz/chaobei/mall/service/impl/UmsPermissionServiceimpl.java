package xyz.chaobei.mall.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.chaobei.common.exception.Asserts;
import xyz.chaobei.mall.mapper.UmsPermissionMapper;
import xyz.chaobei.mall.model.UmsPermissionModel;
import xyz.chaobei.mall.pojo.UmsPermissionPageAO;
import xyz.chaobei.mall.pojo.UmsPermissionSaveAO;
import xyz.chaobei.mall.service.UmsPermissionService;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 权限管理 Service impl
 *
 * @author mrc
 * @since 2020-10-29 17:16:37
 */
@Service
public class UmsPermissionServiceimpl implements UmsPermissionService {

    @Autowired
    private UmsPermissionMapper umsPermissionMapper;

    @Override
    public List<UmsPermissionModel> findAll() {
        return umsPermissionMapper.selectList(null);
    }

    @Override
    public UmsPermissionModel findOne(Integer id, boolean isExc) {

        UmsPermissionModel result = umsPermissionMapper.selectById(id);

        if (Objects.isNull(result) && isExc) {
            Asserts.fail("未查询到相关信息");
        }
        return result;
    }

    @Override
    public Page<UmsPermissionModel> findPage(UmsPermissionPageAO pageAO) {

        Page page = new Page(pageAO.getCurrent(), pageAO.getSize());
        QueryWrapper wrapper = new QueryWrapper();

        wrapper.eq("`name`", pageAO.getName());
        wrapper.eq("`permission`", pageAO.getPermission());
        wrapper.eq("`category`", pageAO.getCategory());

        umsPermissionMapper.selectPage(page, wrapper);

        return page;
    }

    @Override
    public boolean save(UmsPermissionSaveAO params) {

        UmsPermissionModel model = new UmsPermissionModel();
        BeanUtils.copyProperties(params, model);
        /**
         * 你的逻辑写在这里
         */
        int num = umsPermissionMapper.insert(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean updateById(UmsPermissionSaveAO params, Integer id) {

        UmsPermissionModel model = new UmsPermissionModel();
        BeanUtils.copyProperties(params, model);

        /**
         * 你的逻辑写在这里
         */
        model.setId(id);
        int num = umsPermissionMapper.updateById(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean deleteById(Integer id) {

        /**
         * 你的逻辑写在这里
         */
        int num = umsPermissionMapper.deleteById(id);
        return SqlHelper.retBool(num);
    }

    @Override
    public List<String> queryPermissionByIds(List<Integer> ids) {
        return umsPermissionMapper.selectBatchIds(ids)
                .stream().map(item -> item.getPermission()).collect(Collectors.toList());
    }

}
