package xyz.chaobei.mall.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import xyz.chaobei.common.exception.Asserts;
import xyz.chaobei.mall.mapper.UmsAdminMapper;
import xyz.chaobei.mall.model.UmsAdminModel;
import xyz.chaobei.mall.model.UmsRoleModel;
import xyz.chaobei.mall.pojo.*;
import xyz.chaobei.mall.security.component.DefaultTokenServer;
import xyz.chaobei.mall.security.config.JwtConfig;
import xyz.chaobei.mall.service.*;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * 后台用户 Service impl
 *
 * @author mrc
 * @since 2020-10-11 20:39:11
 */
@Service
public class UmsAdminServiceimpl implements UmsAdminService {

    @Autowired
    private UmsAdminMapper umsAdminMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private DefaultTokenServer defaultTokenServer;
    @Autowired
    private JwtConfig jwtConfig;
    @Autowired
    private UmsRoleService umsRoleService;
    @Autowired
    private UmsRoleResourceRelationService roleResourceRelationService;
    @Autowired
    private UmsResourceService resourceService;

    @Override
    public UmsAdminModel findOne(Integer id, boolean isExc) {

        UmsAdminModel result = umsAdminMapper.selectById(id);

        if (Objects.isNull(result) && isExc) {
            Asserts.fail("未查询到相关信息");
        }
        return result;
    }
    @Override
    public List<UmsAdminModel> findAll() {
        return umsAdminMapper.selectList(null);
    }

    @Override
    public Page<UmsAdminModel> findPage(UmsAdminPageAO pageAO) {

        Page page = new Page(pageAO.getCurrent(), pageAO.getSize());
        QueryWrapper wrapper = new QueryWrapper();

        umsAdminMapper.selectPage(page, wrapper);

        return page;
    }

    @Override
    public boolean save(UmsAdminSaveAO params) {

        UmsAdminModel model = new UmsAdminModel();
        BeanUtils.copyProperties(params, model);
        /**
         * 你的逻辑写在这里
         */
        int num = umsAdminMapper.insert(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean updateById(UmsAdminSaveAO params, Integer id) {

        UmsAdminModel model = new UmsAdminModel();
        BeanUtils.copyProperties(params, model);

        /**
         * 你的逻辑写在这里
         */
        model.setId(id);
        int num = umsAdminMapper.updateById(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean deleteById(Integer id) {

        /**
         * 你的逻辑写在这里
         */
        int num = umsAdminMapper.deleteById(id);
        return SqlHelper.retBool(num);
    }

    @Override
    public UmsAdminTokenBO umsAdminLogin(UmsAdminLoginParam param) {

        // 通过用户名获取userDetail
        UserDetails userDetails = this.findUserDetailByUserName(param.getUsername());
        // 基本校验用户名和密码
        if (!passwordEncoder.matches(param.getPassword(), userDetails.getPassword())) {
            Asserts.fail("用户名密码错误");
        }

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        // 将构建的用户信息加入spring security context 上下文
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = defaultTokenServer.generateToken(userDetails);

        return UmsAdminTokenBO.builder().token(token).tokenHeader(jwtConfig.getTokenHeader()).build();
    }

    @Override
    public UserDetails findUserDetailByUserName(String username) {

        UmsAdminModel dbUser = this.findUserByUserName(username);

        // 基本校验用户名和密码
        if (null == dbUser) {
            Asserts.fail("用户名不存在");
        }
        // 通过多个角色id 查询未被锁定的角色信息。
        List<UmsRoleModel> roleModels = umsRoleService.queryNormalRoleByUserId(dbUser.getId());

        Set<String> permissions = new HashSet<>();

        for (UmsRoleModel item : roleModels) {
            // 查询单个角色的多个权限id
            // List<Integer> permissionIds = umsRolePermissionRelationService.queryPermissionIdsByRoleId(item.getId());
            // 通过权限id 查询权限字符
            // List<String> permission = umsPermissionService.queryPermissionByIds(permissionIds);
            // 加入set 集合、不会重复
            // permissions.addAll(permission);

            // 使用新的权限字符方式实现，或者两种方式均可以实现
            List<Integer> resourceIds = roleResourceRelationService.queryResourceIdsByRoleId(item.getId());

            if (resourceIds.isEmpty()) {
                continue;
            }

            List<String> resources = resourceService.queryResourceByIds(resourceIds);
            permissions.addAll(resources);
        }
        return new UmsAdminUserDetails(dbUser, permissions);
    }

    @Override
    public UmsAdminModel findUserByUserName(String username) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("username", username);

        UmsAdminModel dbUser = umsAdminMapper.selectOne(wrapper);

        return dbUser;
    }

}
