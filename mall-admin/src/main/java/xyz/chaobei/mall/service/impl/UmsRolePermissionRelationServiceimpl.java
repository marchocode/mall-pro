package xyz.chaobei.mall.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.chaobei.mall.mapper.UmsRolePermissionRelationMapper;
import xyz.chaobei.mall.model.UmsRolePermissionRelationModel;
import xyz.chaobei.mall.pojo.UmsRolePermissionRelationPageAO;
import xyz.chaobei.mall.pojo.UmsRolePermissionRelationSaveAO;
import xyz.chaobei.mall.service.UmsRolePermissionRelationService;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 角色关联权限 Service impl
 *
 * @author mrc
 * @since 2020-10-29 17:32:03
 */
@Service
public class UmsRolePermissionRelationServiceimpl implements UmsRolePermissionRelationService {

    @Autowired
    private UmsRolePermissionRelationMapper umsRolePermissionRelationMapper;

    @Override
    public boolean save(UmsRolePermissionRelationSaveAO params) {

        UmsRolePermissionRelationModel model = new UmsRolePermissionRelationModel();
        BeanUtils.copyProperties(params, model);
        /**
         * 你的逻辑写在这里
         */
        int num = umsRolePermissionRelationMapper.insert(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean deleteById(Integer id) {

        /**
         * 你的逻辑写在这里
         */
        int num = umsRolePermissionRelationMapper.deleteById(id);
        return SqlHelper.retBool(num);
    }

    @Override
    public List<Integer> queryPermissionIdsByRoleId(Integer id) {
        return umsRolePermissionRelationMapper
                .selectList(new QueryWrapper<UmsRolePermissionRelationModel>().eq("role_id", id))
                .stream().map(item -> item.getPermissionId()).collect(Collectors.toList());
    }

}
