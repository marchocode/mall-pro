package xyz.chaobei.mall.service;

import org.springframework.transaction.annotation.Transactional;
import xyz.chaobei.mall.pojo.UmsAdminRoleRelationSaveAO;

import java.util.List;

/**
 * 用户关联角色 Service
 *
 * @author mrc
 * @since 2020-10-29 16:36:51
 */
public interface UmsAdminRoleRelationService {
    /**
     * <p>保存一个用户关联角色
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @return boolean
     * @since 2020-10-29 16:36:51
     **/
    boolean save(UmsAdminRoleRelationSaveAO params);

    /**
     * <p>删除一个用户关联角色
     * <p>author: mrc
     *
     * @param id 被删除的ID 信息
     * @return boolean
     * @since 2020-10-29 16:36:51
     **/
    boolean deleteById(Integer id);

    /**
     * <p>查询用户所有的角色id 集合
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param id 用户id
     * @return java.util.List<java.lang.Integer>
     * @since 2020/10/29
     **/
    List<Integer> queryAdminRoleIds(Integer id);

    /**
     * <p>
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param userId  用户id
     * @param roleIds 角色ID
     * @return boolean
     * @since 2021/3/17
     **/
    @Transactional(rollbackFor = Exception.class)
    boolean updateAdminRole(Integer userId, List<Integer> roleIds);
}
