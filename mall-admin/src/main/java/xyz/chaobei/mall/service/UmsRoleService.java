package xyz.chaobei.mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import xyz.chaobei.mall.model.UmsRoleModel;
import xyz.chaobei.mall.pojo.UmsRoleSaveAO;
import xyz.chaobei.mall.pojo.UmsRolePageAO;

import java.util.List;

/**
 * 角色管理 Service
 *
 * @author mrc
 * @since 2020-10-29 14:06:03
 */
public interface UmsRoleService {

    /**
     * <p>查询所有角色管理
     * <p>author: mrc
     *
     * @return java.util.List<xyz.chaobei.mall.model.UmsRoleModel>
     * @since 2020-10-29 14:06:03
     **/
    List<UmsRoleModel> findAll();

    /**
     * <p>查询单个角色管理
     * <p>author: mrc
     *
     * @return xyz.chaobei.mall.model.UmsRoleModel
     * @since 2020-10-29 14:06:03
     **/
    @Cacheable(value = "UmsRoleModel",key = "'UmsRoleModel:'+#id")
    UmsRoleModel findOne(Integer id,boolean isExc);

    /**
     * <p>默认分页请求角色管理
     * <p>author: mrc
     *
     * @param pageAO 分页入参查询参数
     * @since 2020-10-29 14:06:03
     * @return xyz.chaobei.mall.model.UmsRoleModel
     **/
    Page<UmsRoleModel> findPage(UmsRolePageAO pageAO);

    /**
     * <p>保存一个角色管理
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @since 2020-10-29 14:06:03
     * @return boolean
     **/
    boolean save(UmsRoleSaveAO params);

    /**
     * <p>修改一个角色管理
     * <p>author: mrc
     *
     * @param id 被修改的ID 信息
     * @param params 被修改的信息
     * @since 2020-10-29 14:06:03
     * @return boolean
     **/
    @CacheEvict(value = "UmsRoleModel",key = "'UmsRoleModel:'+#id")
    boolean updateById(UmsRoleSaveAO params, Integer id);

    /**
     * <p>删除一个角色管理
     * <p>author: mrc
     *
     * @param id 被删除的ID 信息
     * @since 2020-10-29 14:06:03
     * @return boolean
     **/
    @CacheEvict(value = "UmsRoleModel",key = "'UmsRoleModel:'+#id")
    boolean deleteById(Integer id);
    /**
     * <p> 通过多个角色ID 查询多个角色信息。并且是正常角色，不是锁定
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     * @param id 用户ID
     * @since 2020/10/29
     * @return java.util.List<xyz.chaobei.mall.model.UmsRoleModel>
     **/
    List<UmsRoleModel> queryNormalRoleByUserId(Integer id);
}
