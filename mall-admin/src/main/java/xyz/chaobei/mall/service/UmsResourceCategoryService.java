package xyz.chaobei.mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import xyz.chaobei.mall.model.UmsResourceCategoryModel;
import xyz.chaobei.mall.pojo.UmsResourceCategoryBO;
import xyz.chaobei.mall.pojo.UmsResourceCategorySaveAO;
import xyz.chaobei.mall.pojo.UmsResourceCategoryPageAO;

import java.util.List;

/**
 * 资源分类 Service
 *
 * @author mrc
 * @since 2021-03-15 17:01:57
 */
public interface UmsResourceCategoryService {
    /**
     * <p>查询所有资源分类
     * <p>author: mrc
     *
     * @return java.util.List<xyz.chaobei.mall.model.UmsResourceCategoryModel>
     * @since 2021-03-15 17:01:57
     **/
    List<UmsResourceCategoryBO> findAll();

    /**
     * <p>查询单个资源分类
     * <p>author: mrc
     *
     * @param id    查询ID
     * @param isExc 未查询到是否抛出异常
     * @return xyz.chaobei.mall.model.UmsResourceCategoryModel
     * @since 2021-03-15 17:01:57
     **/
    UmsResourceCategoryModel findOne(Integer id, boolean isExc);

    /**
     * <p>默认分页请求资源分类
     * <p>author: mrc
     *
     * @param pageAO 分页入参查询参数
     * @return xyz.chaobei.mall.model.UmsResourceCategoryModel
     * @since 2021-03-15 17:01:57
     **/
    Page<UmsResourceCategoryModel> findPage(UmsResourceCategoryPageAO pageAO);

    /**
     * <p>保存一个资源分类
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @return boolean
     * @since 2021-03-15 17:01:57
     **/
    boolean save(UmsResourceCategorySaveAO params);

    /**
     * <p>修改一个资源分类
     * <p>author: mrc
     *
     * @param id     被修改的ID
     * @param params 被修改的信息
     * @return boolean
     * @since 2021-03-15 17:01:57
     **/
    boolean updateById(UmsResourceCategorySaveAO params, Integer id);

    /**
     * <p>删除一个资源分类
     * <p>author: mrc
     *
     * @param id 被删除的ID
     * @return boolean
     * @since 2021-03-15 17:01:57
     **/
    boolean deleteById(Integer id);
}
