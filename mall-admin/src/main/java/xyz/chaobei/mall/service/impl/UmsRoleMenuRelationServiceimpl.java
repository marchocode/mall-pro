package xyz.chaobei.mall.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.chaobei.mall.mapper.UmsRoleMenuRelationMapper;
import xyz.chaobei.mall.model.UmsRoleMenuRelationModel;
import xyz.chaobei.mall.pojo.UmsRoleMenuRelationPageAO;
import xyz.chaobei.mall.pojo.UmsRoleMenuRelationSaveAO;
import xyz.chaobei.mall.service.UmsRoleMenuRelationService;

import xyz.chaobei.common.exception.Asserts;

import java.util.List;
import java.util.Objects;

/**
 * 角色开通的菜单 Service impl
 *
 * @author mrc
 * @since 2021-01-19 10:10:17
 */
@Service
public class UmsRoleMenuRelationServiceimpl implements UmsRoleMenuRelationService {

    @Autowired
    private UmsRoleMenuRelationMapper umsRoleMenuRelationMapper;

    @Override
    public List<UmsRoleMenuRelationModel> queryByRoleIds(List<Integer> ids) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.in("`role_id`", ids);

        List<UmsRoleMenuRelationModel> result = umsRoleMenuRelationMapper.selectList(wrapper);

        return result;
    }

    @Override
    public List<UmsRoleMenuRelationModel> findAll() {
        return umsRoleMenuRelationMapper.selectList(null);
    }

    @Override
    public UmsRoleMenuRelationModel findOne(Integer id, boolean isExc) {

        UmsRoleMenuRelationModel result = umsRoleMenuRelationMapper.selectById(id);

        if (Objects.isNull(result) && isExc) {
            Asserts.fail("未查询到相关信息");
        }
        return result;
    }

    @Override
    public Page<UmsRoleMenuRelationModel> findPage(UmsRoleMenuRelationPageAO pageAO) {

        Page page = new Page(pageAO.getCurrent(), pageAO.getSize());
        QueryWrapper wrapper = new QueryWrapper();


        umsRoleMenuRelationMapper.selectPage(page, wrapper);

        return page;
    }

    @Override
    public boolean save(UmsRoleMenuRelationSaveAO params) {

        UmsRoleMenuRelationModel model = new UmsRoleMenuRelationModel();
        BeanUtils.copyProperties(params, model);
        /**
         * 你的逻辑写在这里
         */
        int num = umsRoleMenuRelationMapper.insert(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean updateById(UmsRoleMenuRelationSaveAO params, Integer id) {

        UmsRoleMenuRelationModel model = new UmsRoleMenuRelationModel();
        BeanUtils.copyProperties(params, model);

        /**
         * 你的逻辑写在这里
         */
        model.setId(id);
        int num = umsRoleMenuRelationMapper.updateById(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean deleteById(Integer id) {

        /**
         * 你的逻辑写在这里
         */
        int num = umsRoleMenuRelationMapper.deleteById(id);
        return SqlHelper.retBool(num);
    }
    @Override
    public boolean updateRoleList(Integer id, List<Integer> menu) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("`role_id`", id);

        int num = umsRoleMenuRelationMapper.delete(wrapper);

        UmsRoleMenuRelationModel item = new UmsRoleMenuRelationModel();
        for (Integer menuId : menu) {

            item.setId(null);
            item.setMenuId(menuId);
            item.setRoleId(id);

            umsRoleMenuRelationMapper.insert(item);
        }
        return SqlHelper.retBool(num);
    }

}
