package xyz.chaobei.mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.security.core.userdetails.UserDetails;
import xyz.chaobei.mall.model.UmsAdminModel;
import xyz.chaobei.mall.model.UmsPermissionModel;
import xyz.chaobei.mall.pojo.UmsAdminLoginParam;
import xyz.chaobei.mall.pojo.UmsAdminSaveAO;
import xyz.chaobei.mall.pojo.UmsAdminPageAO;
import xyz.chaobei.mall.pojo.UmsAdminTokenBO;

import java.util.List;

/**
 * 后台用户 Service
 *
 * @author mrc
 * @since 2020-10-11 20:39:11
 */
public interface UmsAdminService {

    /**
     * <p>查询单个后台用户
     * <p>author: mrc
     *
     * @return xyz.chaobei.mall.model.UmsPermissionModel
     * @since 2020-10-29 17:16:37
     **/
    UmsAdminModel findOne(Integer id, boolean isExc);
    /**
     * <p>查询所有后台用户
     * <p>author: mrc
     *
     * @return java.util.List<xyz.chaobei.mall.model.UmsAdminModel>
     * @since 2020-10-11 20:39:11
     **/
    List<UmsAdminModel> findAll();

    /**
     * <p>默认分页请求后台用户
     * <p>author: mrc
     *
     * @param pageAO 分页入参查询参数
     * @return xyz.chaobei.mall.model.UmsAdminModel
     * @since 2020-10-11 20:39:11
     **/
    Page<UmsAdminModel> findPage(UmsAdminPageAO pageAO);

    /**
     * <p>保存一个后台用户
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @return boolean
     * @since 2020-10-11 20:39:11
     **/
    boolean save(UmsAdminSaveAO params);

    /**
     * <p>修改一个后台用户
     * <p>author: mrc
     *
     * @param id     被修改的ID 信息
     * @param params 被修改的信息
     * @return boolean
     * @since 2020-10-11 20:39:11
     **/
    boolean updateById(UmsAdminSaveAO params, Integer id);

    /**
     * <p>删除一个后台用户
     * <p>author: mrc
     *
     * @param id 被删除的ID 信息
     * @return boolean
     * @since 2020-10-11 20:39:11
     **/
    boolean deleteById(Integer id);

    /**
     * <p>
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param param 用户名和密码
     * @return 返回token 以及token 头名称
     * @since 2020/10/15
     **/
    UmsAdminTokenBO umsAdminLogin(UmsAdminLoginParam param);

    /**
     * <p>通过用户名构建一个 security userDetails
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param username
     * @return org.springframework.security.core.userdetails.UserDetails
     * @since 2020/10/22
     **/
    UserDetails findUserDetailByUserName(String username);
    /**
     * <p>
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param username 用户名
     * @return xyz.chaobei.mall.model.UmsAdminModel
     * @since 2021/1/19
     **/
    UmsAdminModel findUserByUserName(String username);
}
