package xyz.chaobei.mall.service;

import xyz.chaobei.mall.pojo.UmsRolePermissionRelationSaveAO;

import java.util.List;

/**
 * 角色关联权限 Service
 *
 * @author mrc
 * @since 2020-10-29 17:32:03
 */
public interface UmsRolePermissionRelationService {
    /**
     * <p>保存一个角色关联权限
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @since 2020-10-29 17:32:03
     * @return boolean
     **/
    boolean save(UmsRolePermissionRelationSaveAO params);
    /**
     * <p>删除一个角色关联权限
     * <p>author: mrc
     *
     * @param id 被删除的ID 信息
     * @since 2020-10-29 17:32:03
     * @return boolean
     **/
    boolean deleteById(Integer id);

    /**
     * <p>通过角色id 查询角色的多个权限
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     * @param id
     * @since 2020/10/29
     * @return java.util.List<java.lang.Integer>
     **/
    List<Integer> queryPermissionIdsByRoleId(Integer id);
}
