package xyz.chaobei.mall.service;

import xyz.chaobei.mall.model.UmsMenuModel;
import xyz.chaobei.mall.pojo.UmsMenuSaveAO;
import xyz.chaobei.mall.pojo.UmsMenuTreeBO;

import java.util.List;

/**
 * 菜单管理 Service
 *
 * @author mrc
 * @since 2021-01-15 16:30:41
 */
public interface UmsMenuService {
    /**
     * <p>按照父类递归查询
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param parent 父ID
     * @return java.util.List<xyz.chaobei.mall.model.UmsMenuModel>
     * @since 2021/3/12
     **/
    List<UmsMenuTreeBO> findByParent(Integer parent);

    /**
     * <p>查询单个菜单管理
     * <p>author: mrc
     *
     * @param id    查询ID
     * @param isExc 未查询到是否抛出异常
     * @return xyz.chaobei.mall.model.UmsMenuModel
     * @since 2021-01-15 16:30:41
     **/
    UmsMenuModel findOne(Integer id, boolean isExc);

    /**
     * <p>保存一个菜单管理
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @return boolean
     * @since 2021-01-15 16:30:41
     **/
    boolean save(UmsMenuSaveAO params);

    /**
     * <p>修改一个菜单管理
     * <p>author: mrc
     *
     * @param id     被修改的ID 信息
     * @param params 被修改的信息
     * @return boolean
     * @since 2021-01-15 16:30:41
     **/
    boolean updateById(UmsMenuSaveAO params, Integer id);

    /**
     * <p>删除一个菜单管理
     * <p>author: mrc
     *
     * @param id 被删除的ID 信息
     * @return boolean
     * @since 2021-01-15 16:30:41
     **/
    boolean deleteById(Integer id);

    /**
     * <p>通过管理员ID 获取开通菜单信息
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param ids 通过多个角色匹配出菜单，
     * @return java.util.List<xyz.chaobei.mall.model.UmsMenuModel>
     * @since 2021/1/19
     **/
    List<UmsMenuModel> queryMenuListByRoleIds(List<Integer> ids);
}
