package xyz.chaobei.mall.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.chaobei.common.exception.Asserts;
import xyz.chaobei.mall.mapper.UmsResourceMapper;
import xyz.chaobei.mall.mapper.UmsRoleResourceRelationMapper;
import xyz.chaobei.mall.model.UmsResourceModel;
import xyz.chaobei.mall.model.UmsRoleResourceRelationModel;
import xyz.chaobei.mall.pojo.UmsRoleResourceRelationPageAO;
import xyz.chaobei.mall.pojo.UmsRoleResourceRelationSaveAO;
import xyz.chaobei.mall.service.UmsRoleResourceRelationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 角色关联资源 Service impl
 *
 * @author mrc
 * @since 2021-01-13 14:40:21
 */
@Service
public class UmsRoleResourceRelationServiceimpl implements UmsRoleResourceRelationService {

    @Autowired
    private UmsRoleResourceRelationMapper umsRoleResourceRelationMapper;
    @Autowired
    private UmsResourceMapper umsResourceMapper;

    @Override
    public List<UmsRoleResourceRelationModel> findAll() {
        return umsRoleResourceRelationMapper.selectList(null);
    }

    @Override
    public UmsRoleResourceRelationModel findOne(Integer id, boolean isExc) {

        UmsRoleResourceRelationModel result = umsRoleResourceRelationMapper.selectById(id);

        if (Objects.isNull(result) && isExc) {
            Asserts.fail("未查询到相关信息");
        }
        return result;
    }

    @Override
    public Page<UmsRoleResourceRelationModel> findPage(UmsRoleResourceRelationPageAO pageAO) {

        Page page = new Page(pageAO.getCurrent(), pageAO.getSize());
        QueryWrapper wrapper = new QueryWrapper();

        wrapper.eq("`role_id`", pageAO.getRoleId());

        umsRoleResourceRelationMapper.selectPage(page, wrapper);

        return page;
    }

    @Override
    public boolean save(UmsRoleResourceRelationSaveAO params) {

        UmsRoleResourceRelationModel model = new UmsRoleResourceRelationModel();
        BeanUtils.copyProperties(params, model);
        /**
         * 你的逻辑写在这里
         */
        int num = umsRoleResourceRelationMapper.insert(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean updateById(UmsRoleResourceRelationSaveAO params, Integer id) {

        UmsRoleResourceRelationModel model = new UmsRoleResourceRelationModel();
        BeanUtils.copyProperties(params, model);

        /**
         * 你的逻辑写在这里
         */
        model.setId(id);
        int num = umsRoleResourceRelationMapper.updateById(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean deleteById(Integer id) {

        /**
         * 你的逻辑写在这里
         */
        int num = umsRoleResourceRelationMapper.deleteById(id);
        return SqlHelper.retBool(num);
    }

    @Override
    public List<Integer> queryResourceIdsByRoleId(Integer id) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("`role_id`", id);

        List<UmsRoleResourceRelationModel> result = umsRoleResourceRelationMapper.selectList(wrapper);

        return result.stream().map(item -> item.getResourceId()).collect(Collectors.toList());
    }

    @Override
    public boolean updateRoleResource(Integer roleId, List<Integer> resource) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("`role_id`", roleId);

        int num = umsRoleResourceRelationMapper.delete(wrapper);

        UmsRoleResourceRelationModel model = new UmsRoleResourceRelationModel();

        for (Integer resourceId : resource) {

            model.setId(null);
            model.setRoleId(roleId);
            model.setResourceId(resourceId);

            umsRoleResourceRelationMapper.insert(model);
        }

        return SqlHelper.retBool(num);
    }

}
