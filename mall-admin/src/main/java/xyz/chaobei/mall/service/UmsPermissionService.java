package xyz.chaobei.mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import xyz.chaobei.mall.model.UmsPermissionModel;
import xyz.chaobei.mall.pojo.UmsPermissionSaveAO;
import xyz.chaobei.mall.pojo.UmsPermissionPageAO;

import java.util.List;

/**
 * 权限管理 Service
 *
 * @author mrc
 * @since 2020-10-29 17:16:37
 */
public interface UmsPermissionService {

    /**
     * <p>查询所有权限管理
     * <p>author: mrc
     *
     * @return java.util.List<xyz.chaobei.mall.model.UmsPermissionModel>
     * @since 2020-10-29 17:16:37
     **/
    List<UmsPermissionModel> findAll();

    /**
     * <p>查询单个权限管理
     * <p>author: mrc
     *
     * @return xyz.chaobei.mall.model.UmsPermissionModel
     * @since 2020-10-29 17:16:37
     **/
    UmsPermissionModel findOne(Integer id,boolean isExc);

    /**
     * <p>默认分页请求权限管理
     * <p>author: mrc
     *
     * @param pageAO 分页入参查询参数
     * @since 2020-10-29 17:16:37
     * @return xyz.chaobei.mall.model.UmsPermissionModel
     **/
    Page<UmsPermissionModel> findPage(UmsPermissionPageAO pageAO);

    /**
     * <p>保存一个权限管理
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @since 2020-10-29 17:16:37
     * @return boolean
     **/
    boolean save(UmsPermissionSaveAO params);

    /**
     * <p>修改一个权限管理
     * <p>author: mrc
     *
     * @param id 被修改的ID 信息
     * @param params 被修改的信息
     * @since 2020-10-29 17:16:37
     * @return boolean
     **/
    boolean updateById(UmsPermissionSaveAO params, Integer id);

    /**
     * <p>删除一个权限管理
     * <p>author: mrc
     *
     * @param id 被删除的ID 信息
     * @since 2020-10-29 17:16:37
     * @return boolean
     **/
    boolean deleteById(Integer id);

    /**
     * <p>通过权限id 集合 查询所有的权限集合字符
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     * @param ids id
     * @since 2020/10/29
     * @return java.util.List<java.lang.String>
     **/
    List<String> queryPermissionByIds(List<Integer> ids);
}
