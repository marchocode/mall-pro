package xyz.chaobei.mall.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.chaobei.mall.mapper.UmsResourceCategoryMapper;
import xyz.chaobei.mall.mapper.UmsResourceMapper;
import xyz.chaobei.mall.model.UmsResourceCategoryModel;
import xyz.chaobei.mall.model.UmsResourceModel;
import xyz.chaobei.mall.pojo.UmsResourceCategoryBO;
import xyz.chaobei.mall.pojo.UmsResourceCategoryPageAO;
import xyz.chaobei.mall.pojo.UmsResourceCategorySaveAO;
import xyz.chaobei.mall.service.UmsResourceCategoryService;

import xyz.chaobei.common.WrapperBuilder;
import xyz.chaobei.common.exception.Asserts;
import xyz.chaobei.mall.service.UmsResourceService;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * 资源分类 Service impl
 *
 * @author mrc
 * @since 2021-03-15 17:01:57
 */
@Service
public class UmsResourceCategoryImpl implements UmsResourceCategoryService {

    @Autowired
    private UmsResourceCategoryMapper umsResourceCategoryMapper;
    @Autowired
    private UmsResourceService umsResourceService;

    @Override
    public List<UmsResourceCategoryBO> findAll() {

        List<UmsResourceCategoryModel> types = umsResourceCategoryMapper.selectList(null);
        List<UmsResourceCategoryBO> result = new LinkedList<>();

        for (UmsResourceCategoryModel type : types) {

            UmsResourceCategoryBO bo = new UmsResourceCategoryBO();
            BeanUtils.copyProperties(type, bo);

            List<UmsResourceModel> resources = umsResourceService.queryResourceByCategory(type.getId());
            bo.setResources(resources);

            result.add(bo);
        }
        return result;
    }

    @Override
    public UmsResourceCategoryModel findOne(Integer id, boolean isExc) {

        UmsResourceCategoryModel result = umsResourceCategoryMapper.selectById(id);

        if (Objects.isNull(result) && isExc) {
            Asserts.fail("未查询到相关信息");
        }
        return result;
    }

    @Override
    public Page<UmsResourceCategoryModel> findPage(UmsResourceCategoryPageAO pageAO) {

        Page page = new Page(pageAO.getCurrent(), pageAO.getSize());

        QueryWrapper wrapper = WrapperBuilder.simpleQuery(pageAO);

        umsResourceCategoryMapper.selectPage(page, wrapper);

        return page;
    }

    @Override
    public boolean save(UmsResourceCategorySaveAO params) {

        UmsResourceCategoryModel model = new UmsResourceCategoryModel();
        BeanUtils.copyProperties(params, model);
        /**
         * 你的逻辑写在这里
         */
        int num = umsResourceCategoryMapper.insert(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean updateById(UmsResourceCategorySaveAO params, Integer id) {

        UmsResourceCategoryModel model = new UmsResourceCategoryModel();
        BeanUtils.copyProperties(params, model);

        /**
         * 你的逻辑写在这里
         */
        model.setId(id);
        int num = umsResourceCategoryMapper.updateById(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean deleteById(Integer id) {

        /**
         * 你的逻辑写在这里
         */
        int num = umsResourceCategoryMapper.deleteById(id);
        return SqlHelper.retBool(num);
    }

}
