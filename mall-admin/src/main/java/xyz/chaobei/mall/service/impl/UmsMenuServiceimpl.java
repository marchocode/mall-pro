package xyz.chaobei.mall.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.chaobei.mall.common.enums.HiddenEnums;
import xyz.chaobei.mall.mapper.UmsMenuMapper;
import xyz.chaobei.mall.model.UmsMenuModel;
import xyz.chaobei.mall.model.UmsRoleMenuRelationModel;
import xyz.chaobei.mall.pojo.UmsMenuSaveAO;
import xyz.chaobei.mall.pojo.UmsMenuTreeBO;
import xyz.chaobei.mall.service.UmsMenuService;

import xyz.chaobei.common.exception.Asserts;
import xyz.chaobei.mall.service.UmsRoleMenuRelationService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 菜单管理 Service impl
 *
 * @author mrc
 * @since 2021-01-15 16:30:41
 */
@Service
@Slf4j
public class UmsMenuServiceimpl implements UmsMenuService {

    @Autowired
    private UmsMenuMapper umsMenuMapper;

    @Autowired
    private UmsRoleMenuRelationService umsRoleMenuRelationService;

    @Override
    public List<UmsMenuTreeBO> findByParent(Integer parent) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("`parent_id`", parent);
        wrapper.eq("`hidden`", HiddenEnums.SHOW.getValue());

        List<UmsMenuModel> children = umsMenuMapper.selectList(wrapper);
        List<UmsMenuTreeBO> result = new LinkedList<>();

        for (UmsMenuModel item : children) {

            UmsMenuTreeBO bo = new UmsMenuTreeBO();
            BeanUtils.copyProperties(item,bo);

            List<UmsMenuTreeBO> child = findByParent(item.getId());
            bo.setChildren(child);

            result.add(bo);
        }

        return result;
    }

    @Override
    public UmsMenuModel findOne(Integer id, boolean isExc) {

        UmsMenuModel result = umsMenuMapper.selectById(id);

        if (Objects.isNull(result) && isExc) {
            Asserts.fail("未查询到相关信息");
        }
        return result;
    }

    @Override
    public boolean save(UmsMenuSaveAO params) {

        UmsMenuModel model = new UmsMenuModel();
        BeanUtils.copyProperties(params, model);
        /**
         * 你的逻辑写在这里
         */
        int num = umsMenuMapper.insert(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean updateById(UmsMenuSaveAO params, Integer id) {

        UmsMenuModel model = new UmsMenuModel();
        BeanUtils.copyProperties(params, model);

        /**
         * 你的逻辑写在这里
         */
        model.setId(id);
        int num = umsMenuMapper.updateById(model);

        return SqlHelper.retBool(num);
    }

    @Override
    public boolean deleteById(Integer id) {

        /**
         * 你的逻辑写在这里
         */
        int num = umsMenuMapper.deleteById(id);
        return SqlHelper.retBool(num);
    }

    @Override
    public List<UmsMenuModel> queryMenuListByRoleIds(List<Integer> roles) {

        log.info("queryMenuListByRoleIds[查询多个角色的菜单] roles={}", JSON.toJSONString(roles));

        if (roles.isEmpty()) {
            return new ArrayList<>();
        }

        List<UmsRoleMenuRelationModel> roleMenus = umsRoleMenuRelationService.queryByRoleIds(roles);
        log.info("queryMenuListByRoleIds[查询多个角色的菜单] roleMenus={}", JSON.toJSONString(roleMenus));

        Set<Integer> ids = roleMenus.stream().map(UmsRoleMenuRelationModel::getMenuId).collect(Collectors.toSet());
        log.info("queryMenuListByRoleIds[查询多个角色的菜单] ids={}", JSON.toJSONString(ids));

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.in("id", ids);

        return umsMenuMapper.selectList(wrapper);
    }

}
