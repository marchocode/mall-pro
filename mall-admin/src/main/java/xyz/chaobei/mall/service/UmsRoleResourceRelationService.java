package xyz.chaobei.mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.transaction.annotation.Transactional;
import xyz.chaobei.mall.model.UmsResourceModel;
import xyz.chaobei.mall.model.UmsRoleResourceRelationModel;
import xyz.chaobei.mall.pojo.UmsRoleResourceRelationSaveAO;
import xyz.chaobei.mall.pojo.UmsRoleResourceRelationPageAO;

import java.util.List;

/**
 * 角色关联资源 Service
 *
 * @author mrc
 * @since 2021-01-13 14:40:21
 */
public interface UmsRoleResourceRelationService {

    /**
     * <p>查询所有角色关联资源
     * <p>author: mrc
     *
     * @return java.util.List<xyz.chaobei.mall.model.UmsRoleResourceRelationModel>
     * @since 2021-01-13 14:40:21
     **/
    List<UmsRoleResourceRelationModel> findAll();

    /**
     * <p>查询单个角色关联资源
     * <p>author: mrc
     *
     * @return xyz.chaobei.mall.model.UmsRoleResourceRelationModel
     * @since 2021-01-13 14:40:21
     **/
    UmsRoleResourceRelationModel findOne(Integer id, boolean isExc);

    /**
     * <p>默认分页请求角色关联资源
     * <p>author: mrc
     *
     * @param pageAO 分页入参查询参数
     * @return xyz.chaobei.mall.model.UmsRoleResourceRelationModel
     * @since 2021-01-13 14:40:21
     **/
    Page<UmsRoleResourceRelationModel> findPage(UmsRoleResourceRelationPageAO pageAO);

    /**
     * <p>保存一个角色关联资源
     * <p>author: mrc
     *
     * @param params 保存入参信息
     * @return boolean
     * @since 2021-01-13 14:40:21
     **/
    boolean save(UmsRoleResourceRelationSaveAO params);

    /**
     * <p>修改一个角色关联资源
     * <p>author: mrc
     *
     * @param id     被修改的ID 信息
     * @param params 被修改的信息
     * @return boolean
     * @since 2021-01-13 14:40:21
     **/
    boolean updateById(UmsRoleResourceRelationSaveAO params, Integer id);

    /**
     * <p>删除一个角色关联资源
     * <p>author: mrc
     *
     * @param id 被删除的ID 信息
     * @return boolean
     * @since 2021-01-13 14:40:21
     **/
    boolean deleteById(Integer id);

    /**
     * <p>通过角色ID 查询所有角色的资源信息
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param id 角色ID
     * @return java.util.List<java.lang.Integer>
     * @since 2021/1/13
     **/
    List<Integer> queryResourceIdsByRoleId(Integer id);

    /**
     * <p>修改角色权限
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param id       角色ID
     * @param resource 权限ID
     * @return boolean
     * @since 2021/3/17
     **/
    @Transactional(rollbackFor = Exception.class)
    boolean updateRoleResource(Integer id, List<Integer> resource);
}
