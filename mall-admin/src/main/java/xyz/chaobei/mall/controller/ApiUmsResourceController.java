package xyz.chaobei.mall.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import xyz.chaobei.common.api.CommonResult;
import xyz.chaobei.mall.service.UmsResourceService;
import xyz.chaobei.mall.service.UmsRoleResourceRelationService;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2021/3/12
 */
@Api(tags = "ApiUmsResourceController", description = "资源管理")
@RestController
@RequestMapping("/umsResource")
@Validated
public class ApiUmsResourceController {

    @Autowired
    private UmsRoleResourceRelationService umsRoleResourceRelationService;

    @ApiOperation("查询角色开通的资源")
    @GetMapping("/role/{id}")
    public CommonResult roleResource(@PathVariable("id") @NotNull Integer id) {

        List<Integer> result = umsRoleResourceRelationService.queryResourceIdsByRoleId(id);
        return CommonResult.success(result);
    }
    /**
     * <p>修改角色的开通权限
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param id       角色ID
     * @param resource 资源ID
     * @return xyz.chaobei.common.api.CommonResult
     * @since 2021/3/17
     **/
    @PutMapping("/role/{id}")
    @ApiOperation("修改角色的开通权限")
    public CommonResult updateRoleResource(@PathVariable("id") @NotNull Integer id, @RequestBody List<Integer> resource) {

        boolean isSave = umsRoleResourceRelationService.updateRoleResource(id, resource);
        return CommonResult.success(isSave);
    }

}
