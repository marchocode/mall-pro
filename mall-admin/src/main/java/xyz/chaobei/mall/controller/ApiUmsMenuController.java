package xyz.chaobei.mall.controller;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import xyz.chaobei.common.api.CommonResult;
import xyz.chaobei.mall.model.UmsMenuModel;
import xyz.chaobei.mall.model.UmsRoleMenuRelationModel;
import xyz.chaobei.mall.pojo.UmsMenuSaveAO;
import xyz.chaobei.mall.pojo.UmsMenuTreeBO;
import xyz.chaobei.mall.service.UmsMenuService;
import xyz.chaobei.mall.service.UmsRoleMenuRelationService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 菜单管理请求控制层
 *
 * @author mrc
 * @since 2021-01-15 16:30:41
 */
@Api(tags = "ApiUmsMenuController", description = "菜单管理")
@RestController
@RequestMapping("/umsMenu")
@Validated
@Slf4j
public class ApiUmsMenuController {

    @Autowired
    private UmsMenuService umsMenuService;

    @Autowired
    private UmsRoleMenuRelationService umsRoleMenuRelationService;

    /**
     * <p>查询所有菜单管理
     * <p>author: mrc
     *
     * @return CommonResult
     * @since 2021-01-15 16:30:41
     **/
    @ApiOperation("查询所有菜单管理")
    @GetMapping("/")
    public CommonResult getList() {

        List<UmsMenuTreeBO> allList = umsMenuService.findByParent(0);
        log.info("ApiUmsMenuController getList[查询所有菜单管理] 查询结果={}", JSON.toJSONString(allList));

        return CommonResult.success(allList);
    }

    /**
     * <p>查询角色开通的菜单信息
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param id 角色ID
     * @return xyz.chaobei.common.api.CommonResult
     * @since 2021/3/15
     **/
    @ApiOperation("查询角色开通的菜单信息")
    @GetMapping("/role/{roleId}")
    public CommonResult getRoleList(@PathVariable("roleId") @ApiParam("角色ID") Integer id) {

        List<UmsRoleMenuRelationModel> result = umsRoleMenuRelationService.queryByRoleIds(Arrays.asList(id));
        List<Integer> menus = result.stream().map(UmsRoleMenuRelationModel::getMenuId).collect(Collectors.toList());

        return CommonResult.success(menus);
    }
    /**
     * <p>
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param id 角色ID
     * @return xyz.chaobei.common.api.CommonResult
     * @since 2021/3/17
     **/
    @PutMapping("/role/{roleId}")
    public CommonResult updateRoleList(@PathVariable("roleId") @NotNull @ApiParam("角色ID") Integer id, @RequestBody List<Integer> menu) {

        boolean isDelete = umsRoleMenuRelationService.updateRoleList(id,menu);
        return CommonResult.success(isDelete);
    }
    /**
     * <p>查询单个菜单管理
     * <p>author: mrc
     *
     * @return CommonResult
     * @since 2021-01-15 16:30:41
     **/
    @ApiOperation("查询单个菜单管理")
    @GetMapping("/{id}")
    public CommonResult getOne(@PathVariable("id") @ApiParam("查询的id") Integer id) {

        log.info("ApiUmsMenuController getOne[查询单个菜单管理] 查询的id={}", id);
        UmsMenuModel result = umsMenuService.findOne(id, true);
        log.info("ApiUmsMenuController getOne[查询单个菜单管理] 查询结果={}", JSON.toJSONString(result));

        return CommonResult.success(result);
    }

    /**
     * <p>保存一个菜单管理
     * <p>author: mrc
     *
     * @param params 保存字段
     * @return CommonResult
     * @since 2021-01-15 16:30:41
     **/
    @ApiOperation("保存一个菜单管理")
    @PostMapping("/")
    public CommonResult save(@RequestBody @Valid @ApiParam("保存字段") UmsMenuSaveAO params) {

        log.info("ApiUmsMenuController save[保存一个菜单管理] 保存字段={}", JSON.toJSONString(params));
        boolean isSave = umsMenuService.save(params);
        log.info("ApiUmsMenuController save[保存一个菜单管理] 操作结果={}", isSave);

        return CommonResult.result(isSave);
    }


    /**
     * <p>修改一个菜单管理
     * <p>author: mrc
     *
     * @param id     被修改的ID
     * @param params 被修改的字段
     * @return CommonResult
     * @since 2021-01-15 16:30:41
     **/
    @ApiOperation("修改一个菜单管理")
    @PutMapping("/{id}")
    public CommonResult update(@PathVariable("id") @ApiParam("被修改的ID") Integer id, @Valid @RequestBody @ApiParam("被修改的字段") UmsMenuSaveAO params) {

        UmsMenuModel result = umsMenuService.findOne(id, true);
        log.info("ApiUmsMenuController update[验证菜单管理] 查询结果={}", JSON.toJSONString(result));

        log.info("ApiUmsMenuController update[修改一个菜单管理] 被修改的ID={},被修改的字段={}", id, params);
        boolean isUpdate = umsMenuService.updateById(params, id);
        log.info("ApiUmsMenuController update[修改一个菜单管理] 修改结果={}", isUpdate);

        return CommonResult.result(isUpdate);
    }

    /**
     * <p>删除一个菜单管理
     * <p>author: mrc
     *
     * @param id 被删除的ID
     * @return CommonResult
     * @since 2021-01-15 16:30:41
     **/
    @ApiOperation("删除一个菜单管理")
    @DeleteMapping("/{id}")
    public CommonResult delete(@Valid @NotNull @PathVariable("id") @ApiParam("被删除的ID") Integer id) {

        UmsMenuModel result = umsMenuService.findOne(id, true);
        log.info("ApiUmsMenuController delete[验证菜单管理] 查询结果={}", JSON.toJSONString(result));

        log.info("ApiUmsMenuController delete[删除一个菜单管理] 被删除的ID={}", id);
        boolean isDelete = umsMenuService.deleteById(id);
        log.info("ApiUmsMenuController delete[删除一个菜单管理] 删除结果={}", isDelete);

        return CommonResult.result(isDelete);
    }

}
