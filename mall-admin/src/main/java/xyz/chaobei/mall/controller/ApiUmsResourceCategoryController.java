package xyz.chaobei.mall.controller;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import xyz.chaobei.common.api.CommonResult;
import xyz.chaobei.mall.model.UmsResourceCategoryModel;
import xyz.chaobei.mall.pojo.UmsResourceCategoryBO;
import xyz.chaobei.mall.pojo.UmsResourceCategorySaveAO;
import xyz.chaobei.mall.pojo.UmsResourceCategoryPageAO;
import xyz.chaobei.mall.service.UmsResourceCategoryService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * 资源分类请求控制层
 *
 * @author mrc
 * @since 2021-03-15 17:01:57
 */
@Api(tags = "ApiUmsResourceCategoryController",description = "资源分类")
@RestController
@RequestMapping("/umsResourceCategory")
@Validated
@Slf4j
public class ApiUmsResourceCategoryController {

    @Autowired
    private UmsResourceCategoryService umsResourceCategoryService;

    /**
     * <p>查询所有资源分类
     * <p>author: mrc
     *
     * @return CommonResult
     * @since 2021-03-15 17:01:57
     **/
    @ApiOperation("查询所有资源分类")
    @GetMapping("/")
    public CommonResult getList() {

        List<UmsResourceCategoryBO> allList = umsResourceCategoryService.findAll();
        log.info("ApiUmsResourceCategoryController getList[查询所有资源分类] 查询结果={}", JSON.toJSONString(allList));

        return CommonResult.success(allList);
    }

    /**
     * <p>查询单个资源分类
     * <p>author: mrc
     *
     * @return CommonResult
     * @since 2021-03-15 17:01:57
     **/
    @ApiOperation("查询单个资源分类")
    @GetMapping("/{id}")
    public CommonResult getOne(@PathVariable("id") @ApiParam("查询的id") Integer id) {

        log.info("ApiUmsResourceCategoryController getOne[查询单个资源分类] 查询的id={}", id);
        UmsResourceCategoryModel result = umsResourceCategoryService.findOne(id,true);
        log.info("ApiUmsResourceCategoryController getOne[查询单个资源分类] 查询结果={}", JSON.toJSONString(result));

        return CommonResult.success(result);
    }

    /**
     * <p>分页请求资源分类
     * <p>author: mrc
     *
     * @param pageAO 分页查询参数
     * @since 2021-03-15 17:01:57
     * @return CommonResult
     **/
    @ApiOperation("分页请求资源分类")
    @PostMapping("/page")
    public CommonResult paging(@RequestBody @ApiParam("分页查询参数") UmsResourceCategoryPageAO pageAO) {

        log.info("ApiUmsResourceCategoryController paging[分页请求资源分类] 分页查询参数={}", JSON.toJSONString(pageAO));
        Page<UmsResourceCategoryModel> allList = umsResourceCategoryService.findPage(pageAO);
        log.info("ApiUmsResourceCategoryController paging[分页请求资源分类] 查询结果={}", JSON.toJSONString(allList));

        return CommonResult.success(allList);
    }

    /**
     * <p>保存一个资源分类
     * <p>author: mrc
     *
     * @param params 保存字段
     * @since 2021-03-15 17:01:57
     * @return CommonResult
     **/
    @ApiOperation("保存一个资源分类")
    @PostMapping("/")
    public CommonResult save(@RequestBody @Valid @ApiParam("保存字段") UmsResourceCategorySaveAO params) {

        log.info("ApiUmsResourceCategoryController save[保存一个资源分类] 保存字段={}", JSON.toJSONString(params));
        boolean isSave = umsResourceCategoryService.save(params);
        log.info("ApiUmsResourceCategoryController save[保存一个资源分类] 操作结果={}", isSave);

        return CommonResult.result(isSave);
    }


    /**
     * <p>修改一个资源分类
     * <p>author: mrc
     *
     * @param id 被修改的ID
     * @param params 被修改的字段
     * @since 2021-03-15 17:01:57
     * @return CommonResult
     **/
    @ApiOperation("修改一个资源分类")
    @PutMapping("/{id}")
    public CommonResult update(@PathVariable("id") @ApiParam("被修改的ID") Integer id, @Valid @RequestBody @ApiParam("被修改的字段") UmsResourceCategorySaveAO params) {

        UmsResourceCategoryModel result = umsResourceCategoryService.findOne(id,true);
        log.info("ApiUmsResourceCategoryController update[验证资源分类] 查询结果={}", JSON.toJSONString(result));

        log.info("ApiUmsResourceCategoryController update[修改一个资源分类] 被修改的ID={},被修改的字段={}", id, params);
        boolean isUpdate = umsResourceCategoryService.updateById(params,id);
        log.info("ApiUmsResourceCategoryController update[修改一个资源分类] 修改结果={}", isUpdate);

        return CommonResult.result(isUpdate);
    }

    /**
     * <p>删除一个资源分类
     * <p>author: mrc
     *
     * @param id 被删除的ID
     * @since 2021-03-15 17:01:57
     * @return CommonResult
     **/
    @ApiOperation("删除一个资源分类")
    @DeleteMapping("/{id}")
    public CommonResult delete(@Valid @NotNull @PathVariable("id") @ApiParam("被删除的ID") Integer id) {

        UmsResourceCategoryModel result = umsResourceCategoryService.findOne(id,true);
        log.info("ApiUmsResourceCategoryController delete[验证资源分类] 查询结果={}", JSON.toJSONString(result));

        log.info("ApiUmsResourceCategoryController delete[删除一个资源分类] 被删除的ID={}", id);
        boolean isDelete = umsResourceCategoryService.deleteById(id);
        log.info("ApiUmsResourceCategoryController delete[删除一个资源分类] 删除结果={}", isDelete);

        return CommonResult.result(isDelete);
    }

}
