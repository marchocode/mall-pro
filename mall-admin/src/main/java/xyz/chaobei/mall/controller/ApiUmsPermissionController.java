package xyz.chaobei.mall.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import xyz.chaobei.common.api.CommonResult;
import xyz.chaobei.mall.model.UmsPermissionModel;
import xyz.chaobei.mall.pojo.UmsPermissionSaveAO;
import xyz.chaobei.mall.pojo.UmsPermissionPageAO;
import xyz.chaobei.mall.service.UmsPermissionService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * 权限管理请求控制层
 *
 * @author mrc
 * @since 2020-10-29 17:16:37
 */
@Api(tags = "ApiUmsPermissionController",description = "权限管理")
@RestController
@RequestMapping("/umsPermission")
@Validated
public class ApiUmsPermissionController {

    @Autowired
    private UmsPermissionService umsPermissionService;


    /**
     * <p>查询所有权限管理
     * <p>author: mrc
     *
     * @return xyz.chaobei.common.api.CommonResult
     * @since 2020-10-29 17:16:37
     **/
    @ApiOperation("查询所有权限管理")
    @GetMapping("/")
    public CommonResult getList() {

        List<UmsPermissionModel> allList = umsPermissionService.findAll();
        return CommonResult.success(allList);
    }

    /**
     * <p>查询单个权限管理
     * <p>author: mrc
     *
     * @return xyz.chaobei.common.api.CommonResult
     * @since 2020-10-29 17:16:37
     **/
    @ApiOperation("查询单个权限管理")
    @GetMapping("/{id}")
    public CommonResult getOne(@PathVariable("id") @ApiParam("查询的id") Integer id) {

        UmsPermissionModel result = umsPermissionService.findOne(id,true);
        return CommonResult.success(result);
    }

    /**
     * <p>默认分页请求权限管理
     * <p>author: mrc
     *
     * @param pageAO 分页查询参数
     * @since 2020-10-29 17:16:37
     * @return xyz.chaobei.common.api.CommonResult
     **/
    @ApiOperation("默认分页请求权限管理")
    @PostMapping("/page")
    public CommonResult paging(@RequestBody @ApiParam("分页查询参数") UmsPermissionPageAO pageAO) {

        Page<UmsPermissionModel> allList = umsPermissionService.findPage(pageAO);
        return CommonResult.success(allList);
    }

    /**
     * <p>保存一个权限管理
     * <p>author: mrc
     *
     * @param params 保存字段
     * @since 2020-10-29 17:16:37
     * @return xyz.chaobei.common.api.CommonResult
     **/
    @ApiOperation("保存一个权限管理")
    @PostMapping("/")
    public CommonResult save(@RequestBody @Valid @ApiParam("保存字段") UmsPermissionSaveAO params) {

        boolean isSave = umsPermissionService.save(params);
        return CommonResult.result(isSave);
    }


    /**
     * <p>修改一个权限管理
     * <p>author: mrc
     *
     * @param id 被修改的ID
     * @param params 被修改的字段
     * @since 2020-10-29 17:16:37
     * @return xyz.chaobei.common.api.CommonResult
     **/
    @ApiOperation("修改一个权限管理")
    @PutMapping("/{id}")
    public CommonResult update(@PathVariable("id") @ApiParam("被修改的ID") Integer id, @Valid @RequestBody @ApiParam("被修改的字段") UmsPermissionSaveAO params) {

        boolean isUpdate = umsPermissionService.updateById(params,id);
        return CommonResult.result(isUpdate);
    }

    /**
     * <p>删除一个权限管理
     * <p>author: mrc
     *
     * @param id 被删除的ID
     * @since 2020-10-29 17:16:37
     * @return xyz.chaobei.common.api.CommonResult
     **/
    @ApiOperation("删除一个权限管理")
    @DeleteMapping("/{id}")
    public CommonResult delete(@Valid @NotNull @PathVariable("id") @ApiParam("被删除的ID") Integer id) {

        boolean isDelete = umsPermissionService.deleteById(id);
        return CommonResult.result(isDelete);
    }

}
