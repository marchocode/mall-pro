package xyz.chaobei.mall.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import xyz.chaobei.common.domain.BaseModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 资源管理AO分页查询对象
 *
 * @author mrc
 * @since 2021-01-13 11:11:20
 */
@Getter
@Setter
public class UmsResourcePageAO extends BaseModel {

        
    /**
     * 资源分类ID
     */
    @ApiModelProperty("资源分类ID")
    @TableField("`category_id`")
    private Integer categoryId;

}
