package xyz.chaobei.mall.pojo;

import xyz.chaobei.common.domain.BaseModel;

import lombok.Getter;
import lombok.Setter;

/**
 * 菜单管理AO分页查询对象
 *
 * @author mrc
 * @since 2021-01-15 16:30:41
 */
@Getter
@Setter
public class UmsMenuPageAO extends BaseModel {

                                        
}
