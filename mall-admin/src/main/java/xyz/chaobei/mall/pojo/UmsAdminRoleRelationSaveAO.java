package xyz.chaobei.mall.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import javax.validation.constraints.*;

/**
 * 用户关联角色 Ins/Upd AO对象
 *
 * @author mrc
 * @since 2020-10-29 16:40:53
 */
@Getter
@Setter
public class UmsAdminRoleRelationSaveAO {
        
    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private Integer adminId;
        
    /**
     * 角色id
     */
    @ApiModelProperty("角色id")
    private Integer roleId;
            
}
