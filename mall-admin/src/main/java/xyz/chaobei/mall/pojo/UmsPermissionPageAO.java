package xyz.chaobei.mall.pojo;

import xyz.chaobei.common.domain.BaseModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 权限管理AO分页查询对象
 *
 * @author mrc
 * @since 2020-10-29 17:16:37
 */
@Getter
@Setter
public class UmsPermissionPageAO extends BaseModel {

        
    /**
     * 权限名称
     */
    @ApiModelProperty("权限名称")
    private String name;
        
    /**
     * 权限字符
     */
    @ApiModelProperty("权限字符")
    private String permission;
        
    /**
     * 权限分类
     */
    @ApiModelProperty("权限分类")
    private Integer category;
            
}
