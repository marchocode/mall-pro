package xyz.chaobei.mall.pojo;

import xyz.chaobei.common.domain.BaseModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 角色关联资源AO分页查询对象
 *
 * @author mrc
 * @since 2021-01-13 14:40:21
 */
@Getter
@Setter
public class UmsRoleResourceRelationPageAO extends BaseModel {

        
    /**
     * 
     */
    @ApiModelProperty("角色ID")
    private Integer roleId;
                
}
