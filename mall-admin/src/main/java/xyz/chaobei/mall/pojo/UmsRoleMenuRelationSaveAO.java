package xyz.chaobei.mall.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import javax.validation.constraints.*;

/**
 * 角色开通的菜单 Ins/Upd AO对象
 *
 * @author mrc
 * @since 2021-01-19 10:10:17
 */
@Getter
@Setter
public class UmsRoleMenuRelationSaveAO {
        
    /**
     * 角色id
     */
    @NotNull(message = "角色id不能为空")
    @ApiModelProperty("角色id")
    private Integer roleId;
        
    /**
     * 菜单id
     */
    @NotNull(message = "菜单id不能为空")
    @ApiModelProperty("菜单id")
    private Integer menuId;
            
}
