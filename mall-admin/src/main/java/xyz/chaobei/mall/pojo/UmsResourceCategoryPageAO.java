package xyz.chaobei.mall.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import xyz.chaobei.common.domain.BaseModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 资源分类AO分页查询对象
 *
 * @author mrc
 * @since 2021-03-15 17:01:57
 */
@Getter
@Setter
public class UmsResourceCategoryPageAO extends BaseModel {

                
}
