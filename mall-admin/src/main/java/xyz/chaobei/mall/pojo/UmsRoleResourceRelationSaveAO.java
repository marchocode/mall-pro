package xyz.chaobei.mall.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import javax.validation.constraints.*;

/**
 * 角色关联资源 Ins/Upd AO对象
 *
 * @author mrc
 * @since 2021-01-13 14:40:21
 */
@Getter
@Setter
public class UmsRoleResourceRelationSaveAO {
        
    /**
     * 
     */
    @NotNull(message = "角色ID不能为空")
    @ApiModelProperty("角色ID")
    private Integer roleId;
        
    /**
     * resource
     */
    @NotNull(message = "资源ID不能为空")
    @ApiModelProperty("资源ID")
    private Integer resourceId;
            
}
