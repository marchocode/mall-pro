package xyz.chaobei.mall.pojo;

import xyz.chaobei.common.domain.BaseModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 角色关联权限AO分页查询对象
 *
 * @author mrc
 * @since 2020-10-29 17:32:03
 */
@Getter
@Setter
public class UmsRolePermissionRelationPageAO extends BaseModel {

                    
}
