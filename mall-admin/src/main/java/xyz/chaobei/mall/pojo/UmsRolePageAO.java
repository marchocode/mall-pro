package xyz.chaobei.mall.pojo;

import xyz.chaobei.common.domain.BaseModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 角色管理AO分页查询对象
 *
 * @author mrc
 * @since 2020-10-29 14:06:03
 */
@Getter
@Setter
public class UmsRolePageAO extends BaseModel {

        
    /**
     * 角色名称
     */
    @ApiModelProperty("角色名称")
    private String name;
                
    /**
     * 0锁定 1正常使用
     */
    @ApiModelProperty("0锁定 1正常使用")
    private Integer lock;
                
}
