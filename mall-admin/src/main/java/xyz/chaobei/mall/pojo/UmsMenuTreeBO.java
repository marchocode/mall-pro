package xyz.chaobei.mall.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.chaobei.mall.model.UmsMenuModel;

import java.util.List;

/**
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2021/3/12
 */
@Getter
@Setter
@ToString
public class UmsMenuTreeBO {

    private Integer id;

    private String title;
    /**
     * 所属子元素
     */
    private List<UmsMenuTreeBO> children;
}
