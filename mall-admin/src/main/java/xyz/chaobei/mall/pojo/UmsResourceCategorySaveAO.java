package xyz.chaobei.mall.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import javax.validation.constraints.*;

/**
 * 资源分类 Ins/Upd AO对象
 *
 * @author mrc
 * @since 2021-03-15 17:01:57
 */
@Getter
@Setter
public class UmsResourceCategorySaveAO {
                
}
