package xyz.chaobei.mall.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import javax.validation.constraints.*;

/**
 * 权限管理 Ins/Upd AO对象
 *
 * @author mrc
 * @since 2020-10-29 17:16:37
 */
@Getter
@Setter
public class UmsPermissionSaveAO {
        
    /**
     * 权限名称
     */
    @NotBlank(message = "权限名称不能为空")
    @ApiModelProperty("权限名称")
    private String name;
        
    /**
     * 权限字符
     */
    @NotBlank(message = "权限字符不能为空")
    @ApiModelProperty("权限字符")
    private String permission;
        
    /**
     * 权限分类
     */
    @NotNull(message = "权限分类不能为空")
    @ApiModelProperty("权限分类")
    private Integer category;
            
}
