package xyz.chaobei.mall.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import javax.validation.constraints.*;

/**
 * 菜单管理 Ins/Upd AO对象
 *
 * @author mrc
 * @since 2021-01-15 16:30:41
 */
@Getter
@Setter
public class UmsMenuSaveAO {
        
    /**
     * 父级ID
     */
    @NotNull(message = "父级ID不能为空")
    @ApiModelProperty("父级ID")
    private Integer parentId;
        
    /**
     * 菜单标题
     */
    @NotBlank(message = "菜单标题不能为空")
    @ApiModelProperty("菜单标题")
    private String title;
        
    /**
     * 菜单级别
     */
    @NotNull(message = "菜单级别不能为空")
    @ApiModelProperty("菜单级别")
    private Integer level;
        
    /**
     * 菜单排序
     */
    @NotNull(message = "菜单排序不能为空")
    @ApiModelProperty("菜单排序")
    private Object sort;
        
    /**
     * 前端VUE 名称
     */
    @NotBlank(message = "前端VUE 名称不能为空")
    @ApiModelProperty("前端VUE 名称")
    private String name;
        
    /**
     * 图标
     */
    @NotBlank(message = "图标不能为空")
    @ApiModelProperty("图标")
    private String icon;
                
}
