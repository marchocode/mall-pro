package xyz.chaobei.mall.pojo;

import xyz.chaobei.common.domain.BaseModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;

/**
 * 角色开通的菜单AO分页查询对象
 *
 * @author mrc
 * @since 2021-01-19 10:10:17
 */
@Getter
@Setter
public class UmsRoleMenuRelationPageAO extends BaseModel {

                    
}
