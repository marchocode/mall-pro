package xyz.chaobei.mall.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import javax.validation.constraints.*;

/**
 * 角色管理 Ins/Upd AO对象
 *
 * @author mrc
 * @since 2020-10-29 14:06:03
 */
@Getter
@Setter
public class UmsRoleSaveAO {
        
    /**
     * 角色名称
     */
    @NotBlank(message = "角色名称不能为空")
    @ApiModelProperty("角色名称")
    private String name;
        
    /**
     * 角色描述
     */
    @NotBlank(message = "角色描述不能为空")
    @ApiModelProperty("角色描述")
    private String description;
                        
}
