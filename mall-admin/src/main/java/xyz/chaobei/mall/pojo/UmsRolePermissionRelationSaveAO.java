package xyz.chaobei.mall.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import javax.validation.constraints.*;

/**
 * 角色关联权限 Ins/Upd AO对象
 *
 * @author mrc
 * @since 2020-10-29 17:32:03
 */
@Getter
@Setter
public class UmsRolePermissionRelationSaveAO {
        
    /**
     * 角色编号
     */
    @ApiModelProperty("角色编号")
    private Integer roleId;
        
    /**
     * 权限编号
     */
    @ApiModelProperty("权限编号")
    private Integer permissionId;
            
}
