package xyz.chaobei.mall.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.chaobei.mall.model.UmsResourceCategoryModel;
import xyz.chaobei.mall.model.UmsResourceModel;

import java.util.List;

/**
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2021/3/16
 */
@Getter
@Setter
@ToString
public class UmsResourceCategoryBO{

    private String name;

    private List<UmsResourceModel> resources;
}
