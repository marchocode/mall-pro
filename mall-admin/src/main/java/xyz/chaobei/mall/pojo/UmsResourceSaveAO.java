package xyz.chaobei.mall.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import javax.validation.constraints.*;

/**
 * 资源管理 Ins/Upd AO对象
 *
 * @author mrc
 * @since 2021-01-13 11:11:20
 */
@Getter
@Setter
public class UmsResourceSaveAO {
        
    /**
     * 资源分类ID
     */
    @NotNull(message = "资源分类ID不能为空")
    @ApiModelProperty("资源分类ID")
    private Integer categoryId;
        
    /**
     * 资源名称
     */
    @NotNull(message = "资源名称不能为空")
    @ApiModelProperty("资源名称")
    private String name;
        
    /**
     * 资源路径
     */
    @NotNull(message = "资源路径不能为空")
    @ApiModelProperty("资源路径")
    private String url;
        
    /**
     * 资源描述
     */
    @ApiModelProperty("资源描述")
    private String description;
            
}
