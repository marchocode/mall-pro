package xyz.chaobei.mall;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.chaobei.mall.pojo.UmsMenuTreeBO;
import xyz.chaobei.mall.service.UmsMenuService;

import java.util.List;

/**
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2020/10/22
 */
@SpringBootTest
@Slf4j
@RunWith(SpringRunner.class)
public class TestController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UmsMenuService umsMenuService;

    @Test
    public void test() {
        String encodePassword = passwordEncoder.encode("123456");
        log.info("password = {}", encodePassword);
    }

    @Test
    public void test2() {
        List<UmsMenuTreeBO> result = umsMenuService.findByParent(0);
        log.info("result = {}", JSON.toJSONString(result));
    }


}
