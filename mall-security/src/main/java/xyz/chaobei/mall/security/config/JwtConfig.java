package xyz.chaobei.mall.security.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 自定义jwt 配置
 *
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2020/10/15
 */
@ConfigurationProperties(prefix = "jwt")
@Getter
@Setter
@Component
public class JwtConfig {
    /**
     * 默认请求头
     */
    private String tokenHeader;
    /**
     * 秘钥
     */
    private String secret;
    /**
     * 请求超时
     */
    private Long expiration;
}
