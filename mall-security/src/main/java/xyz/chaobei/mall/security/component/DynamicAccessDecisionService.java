package xyz.chaobei.mall.security.component;

import org.springframework.security.access.ConfigAttribute;

import java.util.Map;

public interface DynamicAccessDecisionService {
    /**
     * <p>加载通配符和资源
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @return java.util.Map<java.lang.String, org.springframework.security.access.ConfigAttribute>
     * @since 2021/1/13
     **/
    Map<String, ConfigAttribute> loadDataSource();
}
