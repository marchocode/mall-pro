package xyz.chaobei.mall.security.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Iterator;

/**
 * 决策管理器，判断当前的请求是否具有访问权限
 *
 * @author <a href='mailto:maruichao52@gmail.com'>MRC</a>
 * @since 2021/1/13
 */
@Slf4j
public class DynamicAccessDecisionManager implements AccessDecisionManager {

    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
        // 若这个链接没有被管理，则直接通过
        if (configAttributes.isEmpty()) {
            return;
        }
        // 访问某个链接需要的权限
        Iterator<ConfigAttribute> iterator = configAttributes.iterator();
        while (iterator.hasNext()) {
            ConfigAttribute item = iterator.next();
            // 账户本身具有的权限
            for (GrantedAuthority owner : authentication.getAuthorities()) {
                if (owner.getAuthority().equals(item.getAttribute())) {
                    return;
                }
            }
        }
        throw new AccessDeniedException("未获得访问权限");
    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
